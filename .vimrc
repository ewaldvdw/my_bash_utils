set nocompatible
filetype plugin indent on
syntax on
" execute pathogen#infect()
set tabstop=4
set shiftwidth=4
set expandtab
" colorscheme late_evening
set hlsearch
imap <Tab> <C-n>
let g:vimwiki_list = [{'path': '/C/Users/ewald/projects/vimwiki/editables/', 'path_html': '/C/Users/ewald/projects/vimwiki/html/', 'auto_export': 0}]
