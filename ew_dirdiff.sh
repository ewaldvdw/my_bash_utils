#!/bin/bash

# This takes two input arguments which are the paths to the two directories to compare. 

for files in $(diff -rq "$1" "$2" | grep 'differ$' | sed "s/^Files //g;s/ differ$//g;s/ and /:/g"); do
    echo -n "Do you want to edit ${files}? [y/n] : "
    read userchoice
    if [ "${userchoice}" == "y" ]; then
        vimdiff "${files%:*}" "${files#*:}"
    fi
done

