# A helper function to wait for running bash jobs
wait_for () {
    # First argument is the number of jobs to wait for
    # Second argument is a printable message
    jobs_curr=$(jobs -p | wc -l)
    while [ "$jobs_curr" -ge "$1" ]; do
        echo "$2: Waiting for ${jobs_curr} jobs."
        wait -n
        jobs_curr=$(jobs -p | wc -l)
    done
}
