alias gits='git status'
alias gitsu='git status -uno'
alias gitl='git log --all --decorate --graph'
alias gitlns='git log --all --decorate --graph --name-status'
alias gitfa='git fetch'
alias gitfa='git fetch --all'

